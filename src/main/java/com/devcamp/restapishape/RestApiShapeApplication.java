package com.devcamp.restapishape;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiShapeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiShapeApplication.class, args);
	}

}
